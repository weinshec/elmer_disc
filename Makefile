
OUT = ${PWD}/results
BIN = ${PWD}/build
SRC = ${PWD}/src


CXXFLAGS  = -std=c++11 -Wall -Wextra -Wno-long-long
LIBS      = -lboost_program_options -lboost_filesystem -lboost_system
ROOTFLAGS = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS  = $(shell $(ROOTSYS)/bin/root-config --libs)


all: ${OUT}/case.ep ${BIN}/vtkParser


${OUT}/case.ep: case.sif ${BIN}/dynamics
	ElmerSolver

${BIN}/dynamics: ${SRC}/dynamics.f90
	elmerf90 $< -o $@

${BIN}/vtkParser: ${SRC}/vtkParser.cc
	g++ -o $@ $(CXXFLAGS) $(LIBS) $(ROOTFLAGS) $(ROOTLIBS) $<


view: ${OUT}/case.ep
	paraview --data=${OUT}/${PROJECT}...vtk

clean:
	rm -rf ${OUT}/*
	rm -rf ${BIN}/*
