FUNCTION getCapacity(Model, N, T) RESULT(C)
    USE DefUtils
    IMPLICIT None
    TYPE(Model_t) :: Model
    INTEGER :: N
    REAL(KIND=dp) :: T, C
    C = 2.5e-7_dp * T**3
END FUNCTION getCapacity



FUNCTION getConductivity(Model, N, T) RESULT(lambda)
    USE DefUtils
    IMPLICIT None
    TYPE(Model_t) :: Model
    INTEGER :: N
    REAL(KIND=dp) :: T, lambda
    lambda = 1.95_dp * T**3
END FUNCTION getConductivity
