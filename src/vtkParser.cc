/**
 * vtkParser.cc - Parse ElmerSolver output vtk files to ROOT trees
 *
 * Usage:
 *          vtkParser [-o OUTFILE] <File0001.vtk>  ...
 */

#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "TTree.h"
#include "TFile.h"



namespace po  = boost::program_options;
namespace bfs = boost::filesystem;

using FileList = std::vector< std::string >;



struct Point
{
    int                 index;
    double              x,y,z;
    std::vector<double> T;
    std::vector<int>    tstep;
};



bool
lineContains(const std::string& line, const std::string& token)
{
    return (line.find(token) != std::string::npos);
}



std::vector<Point>
readGrid(const std::string& filename)
{
    std::vector<Point>  points;
    std::ifstream       fs;
    std::string         line, dummy;
    size_t              nPoints = 0;

    fs.open(filename);

    while( std::getline(fs,line) ) {
        if (lineContains(line, "DATASET UNSTRUCTURED_GRID")) {
            fs >> dummy >> nPoints >> dummy;
            break;
        }
    }

    for (size_t i = 0; i < nPoints; i++) {
        Point p;
        p.index = i;
        fs >> p.x >> p.y >> p.z;
        points.push_back(p);
    }

    fs.close();

    return points;
}



int
readStepIndex(const std::string &filename)
{
    bfs::path filepath(filename);
    std::string stem = filepath.stem().string();

    size_t i = stem.rfind('.');
    if (i == std::string::npos) {
        std::cerr << "Inconventional filename " << filename << std::endl;
        return -1;
    }

    return std::stoi(stem.substr(i+1, stem.length() - i));
}



void
readTemperature(const std::string &filename, std::vector<Point>& points)
{
    std::ifstream       fs;
    std::string         line, dummy;
    double              T = 0;

    int tstep = readStepIndex(filename);
    if (tstep != -1) {
        fs.open(filename);

        while( std::getline(fs,line) ) {
            if (lineContains(line, "SCALARS Temperature double")) {
                std::getline(fs,dummy);
                break;
            }
        }

        for (size_t i = 0; i < points.size(); i++) {
            fs >> T;
            points[i].T.push_back(T);
            points[i].tstep.push_back(tstep);
        }

        fs.close();
    } else {
        std::cerr << "==> Skipping file!" << std::endl;
    }
}



size_t
fillTree(TTree& tree, const std::vector<Point> &points)
{
    Point point;
    tree.Branch("index", &point.index);
    tree.Branch("x",     &point.x);
    tree.Branch("y",     &point.y);
    tree.Branch("z",     &point.z);
    tree.Branch("T",     &point.T);
    tree.Branch("tstep", &point.tstep);

    for (const auto& p : points) {
        point = p;
        tree.Fill();
    }

    return points.size();
}



int
main(int argc, char const *argv[])
{
    po::options_description allowedOptions("Allowed options");
    allowedOptions.add_options()
        ("help", "produce this help message")
        ("output,o", po::value<std::string>()->default_value("out.root"),
            "specify output ROOT file");

    po::options_description hiddenOptions("Hidden options");
    hiddenOptions.add_options()
        ("input", po::value<FileList>(), "input vtk files");
    po::positional_options_description p;
    p.add("input", -1);

    po::options_description allOptions("Allowed options");
    allOptions.add(allowedOptions).add(hiddenOptions);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
        options(allOptions).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help") || !vm.count("input")) {
        std::cerr << "\nSyntax:  vtkParser [options] <file.0001.vtk> ...\n"
                  << std::endl << allowedOptions << std::endl;
        return 1;
    }

    std::string outfile = vm["output"].as<std::string>();
    FileList inputFiles = vm["input"].as<FileList>();

    auto points = readGrid(inputFiles[0]);
    for (const auto& f : inputFiles) {
        readTemperature(f, points);
    }

    TFile out(outfile.c_str(), "RECREATE");
    TTree tree("tree", "tree");
    fillTree(tree, points);
    tree.Write();
    out.Close();

    return 0;
}
