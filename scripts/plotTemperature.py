

from array import array
import collections
import numpy as np
import sys
sys.argv.append('-b')

import ROOT
from AtlasStyle import AtlasStyle


Point = collections.namedtuple('Point', ['i', 'x', 'y', 'z', 't', 'T'])



def getPointCloseTo(tree, x=0, y=0, z=0):
    """Return a Point instance closest to given coordinates

    Args:
        tree (ROOT.TTree): tree object containing Points
        x,y,z (float): coordinates within the grid

    Returns:
        Points: Point instance closest to x,y,z
    """

    point = None
    Rmin  = 1e16

    for p in tree:
        d = ((p.x-x)**2 + (p.y-y)**2 + (p.z-y)**2)
        if d < Rmin:
            point = Point(p.index, p.x, p.y, p.z,
                          np.array(p.tstep), np.array(p.T))
            Rmin = d

    return point



def plotTvsTime(p):
    """Create a plot showing temperature vs time of a given Point

    Args:
        p (Point): instance of Point

    Returns:
        ROOT.TGraph: resulting plot instance of TGraph
    """
    graph = ROOT.TGraph(len(p.t), array('f', p.t), array('f', p.T*1e3))
    graph.GetXaxis().SetTitle("time / #mus")
    graph.GetYaxis().SetTitle("temperature / mK")
    graph.GetXaxis().SetRangeUser(0,100)

    graph.SetMarkerColor(ROOT.kAzure+7)

    return graph



if __name__ == '__main__':

    argv = sys.argv[1:-1] # strip filename and -b option for batchmode

    inFile = ROOT.TFile(argv[0])
    tree   = inFile.Get("tree")

    p0 = getPointCloseTo(tree)
    g1 = plotTvsTime(p0)

    c1 = ROOT.TCanvas('c1','c1', 1000,1000)
    g1.Draw("AP")
    c1.SaveAs("out.pdf")


    inFile.Close()
